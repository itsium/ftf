'use strict';

var fs = require('fs'),
path = require('path');

fs.watchFile(path.resolve(__dirname) + '/configs/prout.json', {
    persistent: true
}, function(event, filename) {
    console.log('Event: ', event);
    console.log('Filename: ', filename);
});