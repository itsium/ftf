### V1:

* Pas de `Fs.ui.Page` ni de `Fs.ui.Header/Footer` etc. On a juste un `Fs.ui.View` et on herite de cette classe en mettant tous les templates a la main dans cette vue.

	Pour les evenements du footer par exemple on copie colle le code dans la vue aussi.

	Idem pour les css, elles sont dans differents dossiers/fichiers mais on les inclues a la main.

	> C'est pas super super a code mais c'est une V1 :) !

	Pour le packager, on le laisse comme il existe deja. Si on a le temps, faire le package.json qui creer l'index.html (mais que celui prod, pas de code js qui parse le package.json pour le dev.).

	Les fichiers css reste en less pour le dev, et on les transforme pour la prod.

	On reste et restera dans l'esprit: "Ne stocker que ce qui est necessaire dans le DOM a savoir tout ce que l'on peut voir ou etre utile pour la vue. Pas de data-role et autre merde."

* [utils/engine.js] Add `async` module option. If this option is set to true, it will perform next module before current module is finished and so on to the next synchrone module. Example:

	[{
		"module": "minifycss",
		"async": true,
		"files": [
			"*.css"
		]
	}, {
		"module": "minifyjs",
		"async": true,
		"files": [
			"*.js"
		]
	}, {
		"module": "combine",
		"async": false, // default option here, just put it explicitly for the example
		"files": {
			"all.css": [
				"*.css"
			],
			"all.js": [
				"*.js"
			]
		}
	}]

* [utils/engine.js, modules/*.js] Add real `finish` callback for modules. For now, when module finished their jobs they simply call a given calback without giving it success [true,false] or errors. We need to change that !

---

### V2:

* Implementation des commentaires `@inline` ou `// <inline>` afin de remplacer un call a une fonction/methode par le code directement de cette methode ou fonction.

	Par exemple:

	    // <inline>
    	var img = Fs.selector.get('body > img');
    	// </inline>

	Sera remplace par:

    	var img = document.querySelectorAll('body > img')[0];

	Ou inversement si une methode a pour commentaire `@inline`, on la cherche dans tout le code pour la remplacer !

* Pour le resize d'image implementer: <https://aheckmann.github.com/gm/>. Ya du blur, du crop (juste width/height, les params x/y sont optionels !) et tout ce dont on reve ! Pour plus d'infos: <http://www.graphicsmagick.org/GraphicsMagick.html#details-crop>

* Faire le `//<debug>` pour que en mode debug on affiche les warnings dut au framework et autre.

* `window.onerror` handler ?

---

### V1000:

* Utiliser `PhantomJS` en mode commande (genre `ftf test/layout monapp/package.json` pour watcher le changement de fichier (ou autre) et lancer un test de layout. En gros:
	1. Charger les routes de l'application les unes apres les autres
	2. Prendre un screenshot de chaque route
	3. Comparer les screens pris avec les precedents et si il y a plus de 5 pixels qui change, alerter le developpeur.

* Utiliser `PhantomJS` en mode commande (genre `ftf test/error monapp/package.json` pour watcher le changement de fichier et lancer un test d'erreur javascript. En gros:
	1. Charger les routes de l'application les unes apres les autres
	2. Alerter le developpeur si `window.onerror` est appelé.

* Quand un element a beaucoup beaucoup de css classes differentes, permettre de les assembler dans une seule classe et de l'appliquer a l'element en question.

* Permettre a qui le veut de faire ses modules pour gerer les alias/inlines qu'il veut et faire la gestion d'extend qu'il veut etc. Plus pour faster than fingers.

* Voir `TODO.md` de `doggybag (faster than fingers - ftf)` pour l'opti a froid du code et les deprecated warning du genre: Votre class css n'est pas opti ou bien ce selecteur pour le DOM query est trop venere etc.

* Permettre de generer une version `noscript` d'un site web. Par exemple un site vitrine, ou tous les templates seraient a la suite ou bien ca voir xtype: "menu" alors on creer un menu statique avec un lien en dur vers chaque template. (home.html, products.html) avec a chaque fois juste le template page et le menu en plus.

### TODO

* Ajouter un module `routine` qui va, par defaut, lorsqu'un fichier `js`/`css`/`html` est copié le minifier, optimiser les images, combiner les `css` et `js` dans un seul fichier, `base64` les `fonts` et `images` dans le css, creer des sprites etc. Exemple de configuration (celle par defaut):

		{
			"module": "routine",
			"files": {
				"*.js": "minifyjs",
				"*.{jpeg,jpg}": "optimizejpg",
				"*.png": "optimizepng",
				"*.css": ["minifycss", "base64url"],
				"*.html": ["minifyhtml", "inlinecss", "inlinejs"]
			},
			// Permet de desactiver la minification html par defaut
			"exclude": [
				"*.html"
			]
		}

Pour attribuer des changements de configuration module:

	{
		"module": "routine",
		"files": {
			"*.js": {
				"module": "closure-compiler",
				"options": "-v --copy none"
			},
			...
		}
	}

Ou encore:

	{
		"module": "routine",
		"files": {
			"*.js": ["minifyjs", {
				"module": "combine",
				"files": {
					"all.js": "${files}"
				}
			},
			...
		}
	}