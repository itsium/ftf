'use strict';

module.exports = (function() {
    // @public
    var pub = {
        /**
         * Module informations
         */
        name: 'move',
        version: '0.1-dev',
        description: 'Move each destination key path to its value path.',
        author: 'Rémi <remi@itsium.cn>',
        usage: [
            '{',
            '   "module": "move",',
            '   "config": {',
            '      "files": {',
            '          "prod/prod.html": "index.html",',
            '          "javascripts/*.js": "assets/",',
            '          "images/*": "assets/",',
            '          "stylesheets/*.css": "assets/"',
            '      },',
            '      "exclude": [',
            '          "Thumbs.db"',
            '      ]',
            '   }',
            '}'
        ],

        run: function(args, modconf, finish) {
            return false;
            var src_files = dict.keys(modconf.files);
            console.log('src_files: ', src_files);
            fs.globArray(src_files, {
                base: args.dest,
                exclude: modconf.exclude || [],
                callback: function(paths) {
                    log.debug('move/paths: ', paths);
                    var i = paths.length,
                    relative = '',
                    src_file = '',
                    dest_file = '';
                    if (!i) {
                        return finish();
                    }
                    while (i--) {
                        relative = files[i].replace(args.dest, '');
                        fs.createDirectoryTree(args.dest, relative);
                        src_file = args.dest + relative;
                        dest_file = args.dest + modconf.files[relative];
                        log.debug('modconf: ', modconf.files, 'relative: ', relative);
                        log.debug('src_file: "%s", dest_file: "%s".', src_file, dest_file);
                        if (fs.isFile(src_file)) {
                            fs.moveFile(src_file, dest_file,
                                (!i ? (finish ? finish : undefined) : undefined));
                        } else if (!i) {
                            finish();
                        }
                    }
                }
            });
            return true;
        }
    },
    // @private
    fs = require('./../utils/filesystem'),
    log = require('./../utils/log'),
    dict = require('./../utils/dict');

    return pub;
}());