'use strict';

module.exports = (function() {
    // @public
    var pub = {
        /**
         * Module informations
         */
        name: 'copy',
        version: '0.1-dev',
        description: 'Copy given files from source to destination.',
        author: 'Rémi <remi@itsium.cn>',
        usage: [
            '{',
            '   "module": "copy",',
            '   "config": {',
            '      "files": [',
            '          "index.html",',
            '          "*.js",',
            '          "images/*",',
            '          "images/*/*"',
            '      ],',
            '      "exclude": [',
            '          "Thumbs.db",',
            '          ".DS_Store"',
            '      ]',
            '   }',
            '}'
        ],

        setup: function(args) {
            return true;
        },

        help: function(args) {
            log.moduleHelp(this);
        },

        run: function(args, config, finish) {
            fs.globArray(config.files, {
                base: args.source,
                exclude: config.exclude || [],
                callback: function(paths) {
                    var p, length = paths.length, last = length,
                    cb = function(err) {
                        if (err) {
                            return finish(err);
                        } else if (!(--last)) {
                            return finish();
                        }
                    };

                    if (!length) {
                        return finish();
                    }

                    while (length--) {
                        p = paths[length];
                        if (fs.isFile(p)) {
                            _copyFile(p, args, cb);
                        } else {
                            _copyFolder(p, args, cb);
                        }
                    }
                }
            });
            return true;
        }
    },
    // @private
    fs = require('./../utils/filesystem'),
    log = require('./../utils/log');

    function _copyFolder(dir, args, callback) {
        fs.createDirectoryTree(dir.replace(args.source, ''),
            args.dest, function(err) {
                return callback(err);
            });
    };

    function _copyFile(file, args, callback) {
        var relative = file.replace(args.source, ''),
            dirname = require('path').dirname(relative);

        if (dirname === '.') {
            return fs.copyFile(file, args.dest + relative, callback);
        }
        fs.createDirectoryTree(dirname, args.dest, function(err) {
            if (err) {
                callback(err);
            } else {
                fs.copyFile(file, args.dest + relative, callback);
            }
        });
    };

    return pub;
}());