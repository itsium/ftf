# faster than fingers

FTF (faster-than-fingers) is a command-line tool designed to help you automating packaging and optimizations of desktop and mobile web applications (or even websites).

## Requirements

 * [node.js](<http://nodejs.org/>) >= 0.8

## Installation

    npm install ftf

## Vendors

 * [closure-compiler](<http://code.google.com/p/closure-compiler/>)
 * [htmlcompressor](<http://code.google.com/p/htmlcompressor/>) >= 1.5.3
 * [yuicompressor](<http://yui.github.com/yuicompressor/>) >= 2.4.7

## Current version

0.1-dev

## Example usage

    ./ftf -s source-folder -d destination-folder -c config-file [-h] [-v]

## Options

	    -h, --help           output usage information
    	-V, --version        output the version number
	    -c, --config <path>  JSON file configuration
	    -s, --source <path>  Source folder path
    	-d, --dest <path>    Destination folder path
	    -v, --verbose        Enable verbose mode

## Example of configuration

    [{
        "module": "copy",
        "config": {
        	"files": ["*"]
		}
    }, {
        "module": "minifyjs",
        "config": {
        	"files": ["*.js"]
		}
    }, {
        "module": "minifycss",
        "config": {
        	"files": ["*.css"]
		}
    }, {
        "module": "minifyhtml",
        "config": {
        	"files": ["*.html"]
		}
    }]

You may also see the [`configs/doc.json`](../configs/doc.json "configs/doc.json") file used to minifying this documentation:

    ./ftf -s examples/doc/ -d doc/ -c configs/doc.json

## Commands

### modules/setup

Show required vendors for each modules. If vendors are installed, setup message will be green for this module, else it will be red and show missing vendors.

#### Usage

	./ftf modules/setup

#### Output



## Modules

### copy

Copy given files from `-s` directory to `-d` directory.
You must use this module to copy given files from `-s` folder to `-d` folder.

#### Config

List of strings matching files.

`Glob` are **supported**.

#### Example

    {
        "module": "copy",
        "config": {
        	"files": [
	            "index.html",
    	        "*.js",
        	    "images/*",
            	"images/*/*"
			],
			"exclude": [
				"Thumbs.db",
				"*.DS_Store*"
			]
        }
    }

* * * *

### Changes v0.1

 * [✓] Add `copy` module.
 * [-] Add `move` module.
 * [-] Add `minifycss` module.
 * [-] Add `minifyhtml` module.
 * [-] Add `minifyjs` module.
 * [-] Add `stats` module.
 * [-] Add `manifest` module.
