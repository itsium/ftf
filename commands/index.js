var cache = false;

function find(dirname) {
    var path = require('path'),
    fs = require('fs'),
    paths = fs.readdirSync(dirname),
    length = paths.length,
    i = -1;

    cache = {};
    dirname = path.resolve(dirname) + '/';
    while (++i < length) {
        var p = dirname + paths[i];
        if (!p.match(module.parent.id) ||
            module.parent.id === '.') {
            var filename = p.split(dirname)[1].split('.js')[0];
            if (filename !== 'index') {
                cache[filename] = require(p);
            }
        }
    }
    return cache;
}

exports = module.exports = cache || find(__dirname);
