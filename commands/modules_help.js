/* modules/help <module_name> */

module.exports = (function() {
    var modules = require('./../modules'),
    log = require('./../utils/log');

    return {
        name: 'modules/help <module>',
        description: 'Show help for given module.',

        action: function(module_name) {
            var mod = modules[module_name];
            if (!mod) {
                log.error(['Module "', module_name,
                    '" does not exists.'].join(''));
            } else if (mod.help) {
                mod.help();
            } else {
                if (mod.name && mod.usage) {
                    log.moduleHelp(mod);
                } else {
                    log.error(['No help found for mod "',
                        module_name, '".'].join(''));
                }
            }
            // @TODO remove exit and deny standart
            // execution if command is executed
            process.exit(0);
        }
    };
}());