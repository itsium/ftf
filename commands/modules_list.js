/* modules/list command */

module.exports = (function() {
    var modules = require('./../modules');

    return {
        name: 'modules/list',
        description: 'List all available modules.',

        action: function() {
            var mname, module, line, separator, length;

            for (mname in modules) {
                module = modules[mname];
                separator = '';
                length = module.name.length;
                while (length++ < 22) {
                    separator += ' ';
                }
                console.log('  %s%s%s', module.name, separator, module.description);
            }
            process.exit(0);
        }
    };
}());