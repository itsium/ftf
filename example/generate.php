<?php

$output = <<<EOD
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <title>mic.packer</title>

    <link rel="stylesheet" href="stylesheets/fonts.css">
    <link rel="stylesheet" href="stylesheets/styles.css">
    <link rel="stylesheet" href="stylesheets/pygment_trac.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="wrapper">
      <header>
        <h1>mic.packer</h1>
        <p>Web site/application packer</p><!--
        <p class="view"><a href="http://github.com/orderedlist/minimal">View the Project on GitHub <small>orderedlist/minimal</small></a></p>
        <ul>
          <li><a href="https://github.com/orderedlist/minimal/zipballmus/master">Download <strong>ZIP File</strong></a></li>
          <li><a href="https://github.com/orderedlist/minimal/tarball/master">Download <strong>TAR Ball</strong></a></li>
          <li><a href="http://github.com/orderedlist/minimal">Fork On <strong>GitHub</strong></a></li>
        </ul>-->
      </header>
      <section>
EOD;
$content = file_get_contents(realpath(dirname(__FILE__) . '/../../') . '/README.md');
require 'markdown.php';

$html = Markdown($content);
$output .= $html;

$output .= <<<EOD
      </section>
      <footer>
        <p>This project is maintained by <a href="mailto:bydooweedoo@gmail.com">bydooweedoo</a></p>
        <p><small>&mdash; Theme by <a href="https://github.com/orderedlist">orderedlist</a></small></p>
      </footer>
    </div>
    <script src="javascripts/scale.fix.js"></script>
  </body>
</html>
EOD;

file_put_contents(realpath(dirname(__FILE__)) . '/index.html', $output);

?>