module.exports = (function() {
    // @private
    var colors = {
        red: '\u001b[0;31m',
        green: '\u001b[0;32m',
        yellow: '\u001b[0;33m',
        blue: '\u001b[0;34m',
        purple: '\u001b[0;35m',
        lightblue: '\u001b[0;36m',
        gray: '\u001b[0;37m',
        reset: '\u001b[0m'
    },
    strings = {
        fileMissing: '%s file is missing.',
        fileDoesNotExists: 'Given %s file "%s" does not exists.',
        fileNotAFile: 'Given %s file "%s" is not a file.',
        directoryMissing: '"%s" directory is missing.',
        directoryDoesNotExists: 'Given %s directory "%s" does not exists.',
        directoryNotADirectory: 'Given %s directory "%s" is not a file.',
        configEmpty: 'Configuration file content is empty.',
        configParseError: 'Configuration file parsing error: \n\n\t%s',
        moduleKeyMissing: 'You must specified a "module" key at JSON block number "%s".',
        moduleConfigKeyMissing: 'You must specified a "config" key for module "%s" at JSON block number "%d".',
        moduleDoesNotExists: 'Module "%s" does not exists at JSON block number "%d".',
        fsDirectoryCreateFailed: 'Failed to create directory "%s".',
        fsFileReadFailed: 'Failed to read file "%s".',
        fsFileWriteFailed: 'Failed to write file "%s".',
        fsFileMoveFailed: 'Failed to move "%s" to "%s".'
    },
    block = '',
    prefix = ' ';

    function color(text, color_name) {
        return [colors[color_name] || '', text, colors.reset].join('');
    }

    return {
        isverbose: false,
        isdebug: false,

        info: function(msg) {
            if (this.isverbose) {
                console.log([prefix, color('info', 'lightblue'),
                    '\t', block, msg].join(''));
            }
        },

        debug: function() {
            if (this.isdebug) {
                var i = 0, length = arguments.length, args = [];

                args.push([prefix, color('debug', 'purple'),
                    '\t', arguments[0]].join(''));
                while (++i < length) {
                    args.push(arguments[i]);
                }
                console.log.apply(console, args);
            }
        },

        memory: function() {
            if (this.isdebug) {
                var mem = process.memoryUsage(), div = 1024.0,
                _prefix = [prefix, color('debug', 'purple'),
                '\t'].join('');
                console.log(['%sResident: %sKo\t',
                    'Heap total: %sKo\t',
                    'Heap used: %sKo'].join(''),
                    _prefix, Math.round(mem.rss / div),
                    Math.round(mem.heapTotal / div),
                    Math.round(mem.heapUsed / div));
            }
        },

        setBlock: function(block_name) {
            block = block_name + '/';
        },

        error: function(errcode) {
            var args = [], length = arguments.length, i = 0;

            args.push([prefix, color('error', 'red'),
                '\t', strings[errcode] || errcode].join(''));
            while (++i < length) {
                args.push(arguments[i]);
            }
            console.log.apply(console, args);
        },

        warning: function(msg) {
            console.log([prefix, color('warning', 'yellow'),
                '\t', block, msg].join(''));
        },

        enter: function(msg) {
            console.log([prefix, color('enter', 'green'),
                ' \t', block, msg].join(''));
        },

        leave: function(msg) {
            console.log([prefix, 'leave \t', block,
                msg].join(''));
        },

        show: function(msg) {
            console.log(['  ', block, msg].join(''));
        },

        success: function(msg) {
            console.log([prefix, color('success', 'green'),
                '\t', block, msg].join(''));
        },

        moduleHelp: function(module) {
            var usage = '\t' + module.usage.join('\n\t');
            console.log(['\n  name: ', (module.name || 'unkown'),
                '\n  version: ', (module.version || 'undefined'),
                '\n  author: ', (module.author || 'N/C'),
                '\n  description: ', (module.description || ''),
                '\n  usage: \n\n', usage, '\n'].join(''));
        }
    };
}());