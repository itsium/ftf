module.exports = (function() {
    return {
        keys: function(dict) {
            var key, keys = [];
            for (key in dict) {
                keys.push(key);
            }
            return keys;
        },

        values: function(dict) {
            var key, keys = [];
            for (key in dict) {
                keys.push(dict[key]);
            }
            return keys;
        }
    };
}());