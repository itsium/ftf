'use strict';
/**
 * filesystem
 *
 * @singleton
 */
module.exports = (function() {
    // @private
    var glob = require('glob'),
    fs = require('fs'),
    path = require('path'),
    existsSync = fs.existsSync,
    exists = fs.exists,
    log = require('./log');

    /**
     * Test if value match patterns.
     *
     * @private
     * @param {Array} patterns List of pattern strings.
     * @param {String} value Value to test.
     * @return {Boolean} True if match else false;
     */
    function matchArray(patterns, value) {
        var regexp, i = patterns.length;
        if (!i) {
            return false;
        }
        while (i--) {
            regexp = new RegExp(patterns[i]);
            if (regexp.test(value)) {
                return true;
            }
        }
        return false;
    }

    return {
        /**
         * Get all matching paths from given patterns and options.
         *
         * @async
         * @method globArray
         * @param {Array} patterns List of {String} with absolute or relative path patterns.
         * @param {Object} opts (optional) Options
         *      - callback {Function} Finish callback function. function(paths) {}
         *      - base {String} (optional) Base path for all paths. Defaults to ''.
         *      - exclude {Array} (optional) List of regexp {String} for excluding path which match.
         */
        globArray: function(patterns, opts) {
            if (!(patterns instanceof Array)) {
                patterns = [patterns];
            }
            if (!(opts instanceof Object)) {
                opts = {};
            }

            var paths = [], length = patterns.length,
                last = length,
                p = '', base = '', callback = opts.callback,
                exclude = opts.exclude || [];

            if (opts.base) {
                base = opts.base;
            }
            while (length--) {
                p = base + patterns[length];
                if (this.isDirectory(p)) {
                    p = path.resolve(p) + path.sep + '*';
                }
                glob(p, {
                    nosort: true
                }, function(err, matches) {
                    if (err) {
                        return callback(err);
                    }
                    matches.forEach(function(v) {
                        if (!matchArray(exclude, v)) {
                            paths.push(v);
                        }
                    });
                    if (!(--last)) {
                        return callback(paths);
                    }
                });
            }
        },

        /**
         * Create missing directory from given path.
         *
         * @async
         * @method createDirectoryTree
         * @param {String} path Path to traverse.
         * @param {String} base (optional) Base path.
         * Use it if path is relative. It will *not* create missing
         * directory from this base path.
         * @param {Function} callback (optional) Callback function calls
         * when finish or when error occured.
         */
        createDirectoryTree: function(p, base, callback) {
            var dirs = p.split(path.sep), length = dirs.length,
            i = -1, dir = '', stop = false;

            if (!length) {
                return false;
            }
            while (++i < length) {
                dir = base + dirs[i];
                if (!existsSync(dir)) {
                    fs.mkdir(dir, (function(count) {
                        return function(err) {
                            if (err) {
                                log.error('fsDirectoryCreateFailed', dir);
                                if (!stop && callback) {
                                    callback(err);
                                } else {
                                    throw err;
                                }
                                stop = true;
                            } else if (!stop && !count && callback) {
                                callback();
                            }
                        };
                    }(i)));
                } else if (!stop && !i && callback) {
                    callback();
                }
            }
        },

        /**
         * Copy given src path to dest path.
         *
         * @async
         * @param {String} src Source path.
         * @param {String} dest Destination path.
         * @param {Function} callback (optional) Callback function calls
         * when finish or when error occured.
         */
        copyFile: function(src, dest, callback) {
            fs.readFile(src, function(err, data) {
                if (err) {
                    log.error('fsFileReadFailed', src);
                    if (callback) {
                        callback(err);
                    } else {
                        throw err;
                    }
                    return false;
                }
                fs.writeFile(dest, data, function(err) {
                    if (err) {
                        log.error('fsFileWriteFailed', dest);
                        if (callback) {
                            callback(err);
                        } else {
                            throw err;
                        }
                    } else if (callback) {
                        callback();
                    }
                });
            });
        },

        /**
         * Move given src path to dest path.
         *
         * @async
         * @param {String} src Source path.
         * @param {String} dest Destination path.
         * @param {Function} callback (optional) Callback function calls
         * when finish or when error occured.
         */
        moveFile: function(src, dest, callback) {
            fs.rename(src, dest, function(err) {
                if (err) {
                    log.error('fsFileMoveFailed', src, dest);
                    if (callback) {
                        callback(err);
                    } else {
                        throw err;
                    }
                } else if (callback) {
                    callback();
                }
            });
        },

        /**
         * Remove given file path.
         *
         * @async
         * @param {String} file File path to remove.
         * @param {Function} callback (optional) Callback function calls
         * when finish or when error occured.
         */
        removeFile: function(file, callback) {
            fs.unlink(file, function(err) {
                if (err) {
                    if (callback) {
                        callback(err);
                    } else {
                        throw err;
                    }
                } else if (callback) {
                    callback();
                }
            });
        },

        /**
         * Test if given path is a file.
         *
         * @sync
         * @param {String} path Path.
         * @return {Boolean} True if given path is a file else false;
         */
        isFile: function(p) {
            return existsSync(p) && fs.statSync(p).isFile();
        },

        /**
         * Test if given path is a directory.
         *
         * @sync
         * @param {String} path Path.
         * @return {Boolean} True if given path is a directory else false;
         */
        isDirectory: function(p) {
            return existsSync(p) && fs.statSync(p).isDirectory();
        }
    };
})();