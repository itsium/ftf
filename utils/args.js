'use strict';

var exports = module.exports = new Args;

exports.Args = Args;

function Args() {

    var publicMethods = {
        validate: function() {
            program = require('commander')
            .version('0.1-dev')
            .option('-c, --config <path>', 'JSON file configuration')
            .option('-s, --source <path>', 'Source folder path')
            .option('-d, --dest <path>', 'Destination folder path')
            .option('-v, --verbose', 'Enable verbose mode')
            .option('-D, --debug', 'Enable debug mode');
            initCommands();
            program.parse(process.argv);
            return check.call(this);
        },

        has: function(name) {
            return (typeof(this[name]) !== 'undefined' ||
                typeof(program[name]) !== 'undefined');
        },

        get: function(name) {
            return this[name] || program[name];
        },

        set: function(name, value) {
            this[name] = value;
        }
    },
    log = require('./log'),
    path = require('path'),
    fs = require('fs'),
    str = require('./str'),
    exists = fs.existsSync,
    stat = fs.statSync,
    program = null,
    vars = {};

    function initCommands() {
        var cname, cmd_module, command,
        command_modules = require('./../commands');

        for (cname in command_modules) {
            cmd_module = command_modules[cname];
            command = program.command(cmd_module.name);
            command.description(cmd_module.description);
            command.action(cmd_module.action);
            if (command.init) {
                command.init(program);
            }
        }
    }

    function checkFile(name, desc) {
        desc = desc || name;
        if (!this.has(name)) {
            log.error('fileMissing', str.capitalize(desc));
            return false;
        }
        var file = path.resolve(this.get(name));
        this.set('config', file);
        if (!exists(file)) {
            log.error('fileDoesNotExists', desc, file);
            return false;
        } else if (!stat(file).isFile()) {
            log.error('fileNotAFile', desc, file);
            return false;
        }
        return true;
    }

    function checkDirectory(name, desc) {
        desc = desc || name;
        if (!this.has(name)) {
            log.error('directoryMissing', str.capitalize(desc));
            return false;
        }
        var dir = path.resolve(this.get(name));
        this.set(name, dir + '/');
        if (!exists(dir)) {
            log.error('directoryDoesNotExists', desc, dir);
            return false;
        } else if (!stat(dir).isDirectory()) {
            log.error('directoryNotADirectory', desc, dir);
            return false;
        }
        return true;
    }

    function check() {
        log.isverbose = this.has('verbose');
        log.isdebug = this.has('debug');
        return (checkFile.call(this, 'config', 'configuration') &&
            checkDirectory.call(this, 'source') &&
            checkDirectory.call(this, 'dest', 'destination'));
    }

    return publicMethods;
};