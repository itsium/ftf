var exports = module.exports = new Engine;

exports.Engine = Engine;

function Engine() {

    var publicMethods = {
        configure: function() {
            args = arguments[0];
            return load.call(this, args.config);
        },

        run: function(finish) {
            var i = -1,
            length = config.length,
            modconf = {};

            (function runBlock(err) {
                if (err) {
                    log.error(err);
                    return finish(false);
                }
                if (++i >= length) {
                    // FileSystem::cleanEmptyDirectories($this->_args->getDestination());
                    return finish(true);
                }
                log.setBlock(i);
                modconf = config[i];
                if (!validateConfig(modconf, i)) {
                    return finish(false);
                }
                var mod = require('./../modules')[modconf.module];
                if (!mod) {
                    log.error('moduleDoesNotExists', modconf.module, i);
                    return finish(false);
                }
                log.enter(modconf.module);
                if (!mod.run(args, modconf.config,
                    (modconf.async ? undefined : runBlock))) {
                    return finish(false);
                } else if (modconf.async) {
                    runBlock();
                }
                //log.leave(modconf.module);
            })();
        }
    },
    fs = require('fs'),
    log = require('./log'),
    args = null,
    config = {};

    function load(configFile) {
        var content = fs.readFileSync(configFile);
        try {
            config = JSON.parse(content);
        } catch (error) {
            log.error('configParseError', error);
            return false;
        }
        if (!config.length) {
            log.error('configEmpty');
            return false;
        }
        return true;
    }

    function validateConfig(modconf, blockNb) {
        log.debug('blockNb: ', blockNb);
        if (!modconf.module ||
            !modconf.module.length) {
            log.error('moduleKeyMissing', blockNb);
            return false;
        } else if (!modconf.config ||
            !(modconf.config instanceof Object)) {
            log.error('moduleConfigKeyMissing', modconf.module, blockNb);
            return false;
        }
        return true;
    }

    return publicMethods;
};